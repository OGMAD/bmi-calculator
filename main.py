class Person:
    id = None
    name = None
    weight = None
    height = None
    bmi = None

    def __init__(self, id):
        self.id = id

    def characterization(self):
        self.name = input("Name: ")
        self.weight = input("Weight in kg: ")
        self.height = input("Height in m: ")


def bmi_calc(person):
    person.bmi = float(person.weight) // pow(float(person.height), 2)

    print("The BMI of {} is {}".format(person.name, person.bmi))


def bmi_evaluation(person):
    if person.bmi < 18.5:
        print("{} is Underweight".format(person.name))
    elif person.bmi < 24.9:
        print("{} is Normalweight".format(person.name))
    elif person.bmi < 29.9:
        print("{} is Overweight".format(person.name))
    elif person.bmi < 34.9:
        print("{} has Obesity type 1".format(person.name))
    elif person.bmi < 39.9:
        print("{} has Obesity type 2".format(person.name))
    else:
        print("{} has Obesity type 3".format(person.name))


if __name__ == '__main__':
    count = int(input("How many people to monitor: "))
    persons = [Person(id) for i in range(count)]

    for i in range(count):
        persons[i].characterization()
        print("--------------------------------")

    for j in range(count):
        bmi_calc(persons[j])
        bmi_evaluation(persons[j])
